#include <iostream>
#include "list.h"
#include "list.cpp"
#include <string>
using namespace std;

int main()
{
	//Sample Code
	List<char>  mylist;
    
	mylist.pushToHead('n');
	mylist.pushToHead('e');
	mylist.pushToHead('k');
    mylist.pushToTail('c');
    mylist.pushToTail('o');
    mylist.pushToTail('s');
    mylist.pushToTail('h');
	mylist.print();
    cout<<endl;
    mylist.popHead();
    mylist.popTail();
    mylist.print();
    cout<<endl;
    cout<<mylist.search('e')<<endl;

	//TO DO! Complete the functions, then write a program to test them.
	//Adapt your code so that your list can store data other than characters - how about a template?
	
}