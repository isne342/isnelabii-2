#include <iostream>
#include "list.h"
using namespace std;
template <class T>
List<T>::~List() {
    for(Node<T> *p; !isEmpty(); ) {
        p=head->next;
        delete head;
        head = p;
    }
}
template <class T>
void List<T>::pushToHead(T el)
{
    head = new Node<T>(el, head, 0);
    if(tail==0)
    {
        tail = head;
    }
    else
    {
        head->next->prev = head;
    }
}
template <class T>
void List<T>::pushToTail(T el)
{
    Node<T> *tmp = head;
    while (tmp && tmp->next != NULL) {
        tmp = tmp->next;
    }
    tail = new Node<T>(el);
    tail->next = NULL;
    if (tmp == NULL)
        head = tail;
    else
        tmp->next = tail;
}
template <class T>
T List<T>::popHead()
{
    char el = head->data;
    Node<T> *tmp = head;
    if(head == tail)
    {
        head = tail = 0;
    }
    else
    {
        head = head->next;
    }
    delete tmp;
    return el;
}
template <class T>
T List<T>::popTail()
{
    
    if(head == tail)
    {
        delete head;
        head = tail =0;
    }
    else
    {
        Node<T> *tmp;
        for(tmp=head; tmp->next != tail ; tmp = tmp->next);
        delete tail;
        tail = tmp;
        tail->next = 0;
    }
    return NULL;
}
template <class T>
bool List<T>::search(T el)
{
    Node<T> *tmp;
    tmp = head;
    while (tmp->data != el && tmp->next != NULL) {
        tmp= tmp->next;
    }
    if (tmp->data != el && tmp->next == NULL) {
        return false;
    }
    else
        return true;
}
template <class T>
void List<T>::print()
{
    if(head  == tail)
    {
        cout << head->data;
    }
    else
    {
        Node<T> *tmp = head;
        while(tmp!=tail)
        {
            cout << tmp->data;
            tmp = tmp->next;
        }
        cout << tmp->data;
    }
}